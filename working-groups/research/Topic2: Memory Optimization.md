# Topic2: Memory Optimization

## Motivation:

​	There are many strategies for memory optimization, such as recalculation and host-device memory switching. These strategies further break through the memory bottleneck by increasing the amount of calculation and increase the batchsize. Increasing batchsize can often improve the utilization of GPU and NPU to improve throughput performance.

## Target:

* Adaptive search memory optimization strategy to optimize the overall network performance.

* Or provide a methodological strategy.

  ![memor_opt](memor_opt.PNG)

## Method:

​	We expect the applicant can conduct memory optimization research based on MindSpore, and hope to get your valuable suggestions to MindSpore in the process. We will do our best to improve the capabilities of the MindSpore framework and  provide you with the most powerful technical support.

## How To Join

1. Submit an issue/PR based on community discussion for consultation or claim on related topics
2. Submit your proposal to us by email xxx@huawei.com